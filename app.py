import sqlite3
from peewee import DoesNotExist
from flask import Flask, render_template, request, redirect, abort
from Models import Book, Genre, Author
from Forms import BookForm

app = Flask(__name__)


@app.route('/')
def books_list():
    books = Book.select()
    return render_template('index.html',
                           books=books)


@app.route('/year/<int:pk>/')
def books_by_year(pk):
    try:
        books = Book.select().where(Book.year == pk)
    except DoesNotExist:
        return abort(404)
    return render_template('index.html',
                           books=books)


@app.route('/genre/<int:pk>/')
def books_by_genre(pk):
    try:
        books = Book.select().where(Book.genre == pk)
    except DoesNotExist:
        return abort(404)
    return render_template('index.html',
                           books=books)


@app.route('/author/<int:pk>/')
def books_by_author(pk):
    try:
        books = Book.select().where(Book.author == pk)
    except DoesNotExist:
        return abort(404)
    return render_template('index.html',
                           books=books)


@app.route('/create/author/', methods=['GET', 'POST'])
def add_author():
    if request.method == 'POST':
        Author.create(name=request.form['name'])
        return redirect('/')
    return render_template('add_author.html')


@app.route('/create/genre/', methods=['GET', 'POST'])
def add_genre():
    if request.method == 'POST':
        Genre.create(name=request.form['name'])
        return redirect('/')
    return render_template('add_genre.html')


@app.route('/create/book/', methods=['GET', 'POST'])
def add_book():
    form = BookForm()
    if request.method == 'POST':
        form = BookForm(formdata=request.form)
        if form.validate():
            form.save()
            return redirect('/')
    return render_template('add_book.html', form=form)


@app.route('/update/book/<int:pk>/', methods=['GET', 'POST'])
def update_book(pk):
    try:
        book = Book.select().where(Book.id == pk).get()
    except DoesNotExist:
        return abort(404)
    form = BookForm(obj=book)
    if request.method == 'POST':
        form = BookForm(formdata=request.form)
        if form.validate():
            form.update(pk)
            return redirect('/')
    return render_template('update_book.html',
                           form=form)


if __name__ == '__main__':
    app.run(
        host='127.0.0.1',
        port=5000,
        debug=True
    )
