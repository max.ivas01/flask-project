import wtforms
from Models import Book, Genre, Author


class BookForm(wtforms.Form):
    name = wtforms.StringField(validators=[wtforms.validators.DataRequired()])
    year = wtforms.StringField(validators=[
        wtforms.validators.Regexp(r"\b\d{4}\b", message="Wrong year format"),
        wtforms.validators.DataRequired()
    ])
    author = wtforms.SelectField(
        choices=[(x.id, x.name) for x in Author.select()]
    )
    genre = wtforms.SelectField(
        choices=[(x.id, x.name) for x in Genre.select()]
    )

    def save(self):
        Book.create(name=self.name.data,
                    year=self.year.data,
                    author=self.author.data,
                    genre=self.genre.data)

    def update(self, pk):
        Book.update(name=self.name.data,
                    year=self.year.data,
                    author=self.author.data,
                    genre=self.genre.data
                    ).where(Book.id == pk).execute()
